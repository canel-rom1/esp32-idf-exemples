/*
 * Créer un Timer simple. Il va lire la valeur toutes les secondes
 */
#include <stdio.h>
#include "esp_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/timer.h"

#define TIMER_DIVIDER         16  //  Hardware timer clock divider

/*
 * Initialisation du Timer.
 */
static void my_timer_init(void)
{
    timer_config_t config;
    config.divider = TIMER_DIVIDER;
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en = TIMER_PAUSE;
    config.alarm_en = TIMER_ALARM_DIS;
    config.intr_type = TIMER_INTR_LEVEL;
    config.auto_reload = 1;

    timer_init(TIMER_GROUP_0, TIMER_0, &config);

    /* Initialise la valeur du Timer à 0 */
    timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
}

/*
 * La tâche du Timer. Lit la valeur toutes les secondes et l'affiche.
 */
static void task1(void)
{
    double timer_counter_value;

    printf("start task\n");

    /* Démarre le Timer */
    timer_start(TIMER_GROUP_0, TIMER_0);

    for(int index=0 ; true ; index++)
    {
        /* Lit la valeur du timer */
        timer_get_counter_time_sec(TIMER_GROUP_0, TIMER_0, &timer_counter_value);

        printf("---\n%d\n", index);
        printf("Value: %lf\n", timer_counter_value);

        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

/*
 * Programme principal pour exécuter la tâche du Timer
 */
void app_main(void)
{
    my_timer_init();
    xTaskCreate(task1, "task_1", 2048, NULL, 5, NULL);
}
